let button1;
let button2;
let button3;
let ctracker;

function preload(){
  coin = loadImage ('coin.png');
}

function setup() {
  background(100);

  //web cam capture
  let capture = createCapture();
 capture.size(540,480);
 capture.position(0,0);

 let c = createCanvas(540, 480);
 c.position(0,0);

  //setup face tracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);

//like button
  button1 = createButton('like');
  button1.position(140,460);
  button1.style("color","#000000");
  button1.style("background","#99ff99");
  button1.style("border-radius", "50%");
  button1.style("font-size", "20px");

  button1.mousePressed(randomCoin);

//unlike button
  button2 = createButton('remove like');
  button2.position(230,460);
  button2.style("clor","#000000");
  button2.style("background","#ff1a53");
  button2.style("border-radius", "49%");
  button2.style("font-size", "20px");


  button2.mousePressed(clearenceAndText);

//clear button
  button3 = createButton('clear');
  button3.position(390,460);
  button3.style("color","#000000");
  button3.style("background","#66d3ff");
  button3.style("border-radius", "50%");
  button3.style("font-size", "20px");

  button3.mousePressed(clearence);

}

function draw() {
  let positions = ctracker.getCurrentPosition();
  if (positions.length) { //check the availability of web cam tracking
    for (let i=0; i<positions.length; i++) { 
       noStroke();
       fill(204,178,225,10);  //color with alpha value
       //draw ellipse at each position point
       ellipse(positions[i][0], positions[i][1], 5, 5);
    }
  }
}

function randomCoin(){
for (var i=0; i<6;i++){
    image(coin,random(width),random(height),50,50);
  }
}

function clearenceAndText() {
  clear();
  fill(255,0,0);
  textSize(20);
  text('your data has already been sold',100,height/2);
}

function clearence(){
  clear();
}
