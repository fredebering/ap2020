**THE INLIGHTENER**


![Screenshot](miniex4.png)

https://fredebering.gitlab.io/ap2020/All%20mini%20ex/miniex4

**Describe your Design, both conceptually and technically**




My design includes a face-tracking system. The face-tracking system is scanning ones face right away when the program is opened. 
It continues scanning the face throughout the programming and never stops. This was made by downloading a library found at https://github.com/auduno/clmtrackr
I copied the code from Winnie's sketch 05 which was presented in class. I then took the code and modified it to my prefrences and the vision
i had with my program. I changed the colors of the ellipses being drawn around the face to a light purple color. 


    fill(204,178,225,10);  
    ellipse(positions[i][0], positions[i][1], 5, 5);
    
    
There is also three buttons which one can press. The first button is a green like-button. The second one is red and says "remove like". The third one is a blue 
clear button. 
When clicking the like-button a lot of coins appears randomly on the screen, making a statement, that by cliking the like-button you have now contribued with 
your data which big companies can now use to track you on the internet and sell it to other companies for marketing purposes. 
In order to make this i preloaded an image of a coin which i placed before my set up function. I then used a for-loop in order to make the 
program print the coins randomly when the button is pressed


    function randomCoin(){
    for (var i=0; i<6;i++){
    image(coin,random(width),random(height),50,50);
    
    
    
I made the button green because i wanted to make it trustworthy and in our cultur the color green is a very trustworthy color because we associate it with something
that says "it ok" or "go ahead". I also chose this color because it is surpose to represent everything you like on the internet and not only
Facebook, which easily could have been interpreted that way if it was blue, like the Facebook Like-button. 



For the second button i chose to make it red and make it a "remove like" button. I chose the color red because i wanted to make it dangerous and not very "clickable"
in our cultur, red is a color which represents something that one is not surpose to do. It says "no" or "don't do it". I chose this because i wanted to make it look
like that clicking this button was the wrong thing do to. At the same time a wanted to show, that once you have clicked a like-button, you have already
giving big companied a lot of information about yourself and removing the like doesn't help anything. You can not retrieve the data you have been giving. All 
of your data is being captured. This is also the meaning behind the printed text "your data as already been sold". I made this by using a clear-function, so that 
the coins would disppear. I then defined the color of the text, the size and what it should say

    function clearenceAndText() {
    clear();
    fill(255,0,0);
    textSize(20);
    text('your data has already been sold',100,height/2);

The third button is just a plain clear-button which only function is for people exploring the program to start over and clear the screen. It has no symbolic
meaning to the program.

I chose to call my work "THE INLIGHTENER" because it gives people a idea about what it actually means when you click on a like-button anywhere on the internet
and many people might not be aware of this. I also hope that it makes people thing twice about how they act on the internet and that they become 
aware that everything we do is being tracked, analysed and sold to other companies that use our data for marketing purposes - just to give an example. 
Facebook or Instagram gives the consumer a free platform and in exchange you allow them to use all of the data that you leave create when browsing through not 
only Facebook but the whole internet



**What is your sketch? What do you want to explore and/or express? And the cultural perspective**



With my design/program i wanted to show how we a ordinary people are contributing to the like-economy which is also refrenced in the article "
The like economy: Social buttons and the data-intensive web" by Carolin Gerlitz and Anne Helmond. There was especially one sentence that i found really interesting
and was really what inspired me to create this program 
"A click on the Like button transforms users’ affective, positive, spontaneous responses to web content into connections between users and 
web objects and quanta of numbers on the Like counter" p. 1358. 
For me it shows how Facebook is systematicly using the data, which we voluntarily provide to the, and uses it to analyse how we act on the internet
and predict how we might act in the future. I realise, as the article also states, that we have all agreed on the terms and conditions when signing
up to Facebook, but Facebook also knows that no one is going to read and understand the agreement. As mentioned by Søren Pold in Software Studies - a lexicon
by mimicking button from old machines but in realty it is

"It is a software simulation of a function, and this simulation aims to hide its mediated character and acts as if the function were natural or mechanical 
in a straight cause-and-effect relation. Yet it is anything but this: it is conventional, coded, arbitrary, and representational, and as such also related 
to the cultural" Software Studies - a lexicon p. 33

This also shows that we have very little knowlegde about how buttons is being made within the computer. I also shows that culturally we know
very little about software, what it is and how it works. Which is think is a shame, because it really is a great and use able knowlegde to have because
computers and software is such a big part of our everyday life.





