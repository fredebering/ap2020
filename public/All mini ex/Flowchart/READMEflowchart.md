**Miniex about Flowcharts**
**My individual thoughts**

For this miniex about flowcharts i have chosen to create a flowchart of my project in my miniex 6, which was a game. Here is a link to the minex folder where you can read about my program/projecy 


https://gitlab.com/fredebering/ap2020/-/blob/master/public/All%20mini%20ex/miniex6/READMEminiex6.md


The reason that i have chosen to create a flowchart of this program is because i thought i would both be challenging and a unique opertunity for me to be able to show what i wanted to achieve with my game. I where uable to make the game the work when i first created it and i thought that the flowchart would help visualize my thought and what functions i wanted the game to have. I found it quite challenging when trying to create a simple flowchart. The thing i found most challenging was trying to figure out what was relevant and what was not. I wanted to present all of the functions in a understandable way, but not simplify to the point where i felt important function out of the flowchart. Though after making two flowcharts in my studygroup i also find i more easy to qiuckly make the flowchart. I also think that this has something to do with the circumstances. It would have been much easier if i was able to meet up with my group and talk about how the two flowcharts should look and what they should contain. It is a lot harder when someone has to share their screen, because it limits the working/writing part of the flowchart to only one person and it is also difficult for the other people to describe what they think should be changed in the flowchart. 
with this being said i really liked working with other on flowcharts. I think it is nice to get at great discussion about the final project and what thoughts we all had. I also found the flowchart to be a great way in order for us to understand what kind of program we would like to make and what functions the program should have in order for it to work. It's a great way to visualize. This is also something that the text  "The Multiple Meanings of a Flowchart" by  Nathan Ensmenger tallk about. Flowcharts can have multiple function in a work processen and i think it was made clear with this weeks minex. I also found it quite difficult to try and figure out what function we should have i our program. I think this is due to my limited knowlegde about programming and coding.

As i mentioned i really found great value of working with flowcharts in a group because it is a way to visualize the program and i makes it a lot easier for us when we have to start to write the code.
The value of making a flowchart individually is, that it is much quicker and you don't have to check in with others and if they agree with you. 

Flowcharts is also a great way for people, who do not know anything about coding because is great way to visualize how a program work and therefore it become a lot more simple. One could argue that flowharts are boolean are therefore they are very logical. It makes it a lot easier for everyone to undestand them

![Screenshot](flowchartGame.png)


**Group discussion about the finals exam and our ideas**


**The overall concept:**


We think that exploring sexsuality and how one can misuse data (intentionally or unintentionally). We like to explore the fact that nothing is neutral, even though one might think it is and how we put our trust into institutions sometimes without being critical. Both of our problems examine these issues. We want to extract some kind of data from the internet and use this data to criticize or clarify some of these problems or inequalities that are present in today’s society. 


**Ideas for the final project**



**Sex and Salary**


![Screenshot](SexAndSalary1.png)


This program makes the differences in salary depending on gender visible. We want to 
To start with the program defines your gender based on a simplified face recognition technology. Based on your gender the program will then show what your average salary should be. This is based on numbers from Danmarks Statistik (dst.dk). If you’re a woman the program will tell you that you could get a much higher salary being a man - and it then refers you to a website with information about gender change. 
Selecting features in machine learning can be political, even though it is automatic. (Features are what parameters that are measured and used in the model - e.g. what constitutes a female or male face). The issues of constructing features - especially unsupervised clustering of data and automatic feature selection can be hard to control. 
Some of the technical challenges that we might face is regarding feature selection. How should the program be able to determine what features on the face related to women, and what features relates to men. The problem is that feature selection is always subjective in the sense that it collects significant data. There are therefore some ethical issues connected to that. The coherence between the significant data and reality, but one also has to be aware that the coherence might be “wrong”. One also has to be aware that it is not this exact coherence one wants to implement into the program. 
One of the ways to address this problem is that we have to be very aware of how the models have been built and what data has been collected/used. Be aware if the models are transparent. 
We deliberately chose only two genders, even though we are aware that there are over 60 different genders. 


**Sexuality and Rights**


![Screenshot](SexAndRights.png)


What we wanted to criticize with this program is the huge inequality that is between different sexualities. Also the limiting binary choice of hetero or not, as a problematic convension and norm that stille needs attention around the world. 
It is also a comment on a caricu
This program is meant to tell a story about one's life based on different data inputs which the subject gives the program and the program will then tell you a fairytale about your life based on different parameters which are changeable. 
This program makes you choose your sexuality (whether or not you are hetero) and tells you a fairytale based on your input. The fairytale will be affected by the sexality and other info you give to the program.
You are only giving the choice of being hetorosexual or not. So anything but hetoresexual will be treated in another way. If your heterosexual you are presented with a straight forward, heteronormative story about your life - and if you’re anything else you will be presented with all kinds of challenges and misery (based on real conditions about unequal rights, repression and so on). 
These different endings also show how one’s life has a different outcome based on sexuality. 
One of the difficulties that could occur is collecting data from the different countries and their laws about sexuality and putting it all into our own JSON-file. We also have to figure out if it makes more sense to have many different JSON-files in order to better organize our data or to put it in fewer JSON-files. 

