
//I HAVE DIVIDED MY SKETCH INTO DIFFERENT SECTIONS.
//SECTION 1: SHOWS THE DIRECTION OF PURPLE CIRCLE1
//SECTION 2: SHOWS THE ILLUSION OF THE CIRCLES DISAPPEARING
//SECTION 3: THE CIRCLES APPEAR AGAIN
//SECTION 4: PRINTING THE CIRCLES ON TO THE CANVAS
//SECTION 5: DEFINING THE CIRCLES

let x=117,y=175;
let purp2Col;
let blue1Col;
let blue2Col;

function setup() {
createCanvas(400,400);

//definting the colors in order to be able to change them
//in order to make it look like they're disapearing

purp2Col = color(121,116,232);
blue1Col = color(140,165,255);
blue2Col = color(116,171,232);
}

function draw() {
background(204,255,255);
noStroke();

//SECTION 1: The code for making Purple Circle1 move in different directions
if(x <= 185 && y == 175){
  x++;
}
if(y<=240 && x ==186){
y++;
}

if(x>=117 && y ==241){
  x--;
}

if(y<=241 && x == 117){
  y--;
}
//SECTION 2: The code for creating the illusion that the circles are
//disapering. In fact they just change to be the same color as the
//background at a certain point

if(x==185 && y==175){
  purp2Col=color(204,255,255)
}

if(x==186 && y==240) {
  blue1Col = color(204,255,255)
}

if(x==117 && y==240){
  blue2Col=color(204,255,255)
}

//SECTION 3: When Purple Circle1 moves back to it's original point
//the other circles reappear
if(x==117 && y==175){
purp2Col = color(121,116,232)
blue1Col = color (140,165,255)
blue2Col = color(116,171,232)
}

//SECTION 3: Printing the circles onto the canvas
fill(purp2Col);
purpleCircle2();

fill(blue1Col);
blueCircle1();

fill(blue2Col);
blueCircle2();

fill(168,128,225);
e(x,y);

//SECTION 5: defining functions for the circles
function e (x,y) {
fill(168,128,225);
ellipse(x,y,50)
}

function purpleCircle2(){
  //fill(121,116,232)
  ellipse(185,175,50)
}

function blueCircle1(){
  //fill(140,165,255)
  ellipse(185,240,50)
}

function blueCircle2 (){
  //fill(116,171,232)
  ellipse(117,240,50)
}
}
