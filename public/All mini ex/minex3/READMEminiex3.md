![Screenshot](miniex3.png)

https://fredebering.gitlab.io/ap2020/All%20mini%20ex/minex3

**Describe your throbber design, both conceptually and technically.**
My throbber consists of four dots, which, in the way that they're placed on the canvas
forms a rectangle. The purple dot that is placed in the upper left corner is moving in a 
rectangle and "eating" the other dots as it moves around. When the purple dots has returned
to is original position, all of the other dots reappers and the process starts again. 

To make it look like my throbber is moving i used x++,y++,x-- and y--. By using these, i was able
to make the purple dot move in different directions on the sceen. In other for the dot to
know, when it was going to change its direction, i used if-statements. When ever the purple dot
reached that same coordinate as one of the other dots, the direction changed.

if(y<=240 && x ==186){
y++;
}


Likewise i made it look like the moving dot what eating the static dots by changeing the
color of the static dots when ever the moving dot reached the same coordinate as one of them. 
To do this, i also used an if-statement 

if(x==185 && y==175){
  purp2Col=color(204,255,255)
  }


In order to make it look like the whole thing was a "looping" i made all of the dots come
back when the moving dot reached its orginial position.

**What is your sketch? What do you want to explore and/or express?**
With my sketch i wanted to explore the relationsship between "human" temporality and computional temporality
I realised that time is not something that the computer understands, but it is something we have to
make it look like it does. The computer doesn't know, that when we see a throbber appear on the screen, it
means that we are waiting for something. The computer is simple being told, by other things within the computer
that when a certain amount of data can not arrive and write at the same time in the memory, garbage data
will be read at the throbber appers on screen. (Soon, Winnie - Throbber: Executing Mirco-temporal Streams)
This have become clear for me when i was making my throbber, that every command creates an illusion of time and
that the throbber itself has no relation to time.

**A cultural perspective**
Culturally we hate the feeling og wasting time and the throbber has come to represent the ultimate time-waster
It is so annoying beacuse it gives no indication to how long there is left before the program, video ect. starts working
again. I personally think that the throbber come from the user-friendliness-culture that has arised in 
designing interfaces. Designers is under impression that the user whats and needs to know as little as possible
about the computer and what is going on inside it. But the user-friendliness has turned kind of backfired because we don't
understand what is acutally doing on. Maybe stepping back and bringing back the process bar would help the
impatience, because it allows people a real concept of time or at least a way of tracking how long the computer
is processing whatever it needs to process. 
