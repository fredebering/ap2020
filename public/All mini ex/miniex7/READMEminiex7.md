Miniex 7

![ScreenShot](miniex7.png)

https://fredebering.gitlab.io/ap2020/All%20mini%20ex/miniex7


**What are the rules in your generative program and describe how your program performs over time and how are the running of the rules contingently enabled emergent behaviors? What's the role of rules and processes in your work?**

Mads Marschall and I have teamed up for this week’s miniex. Our program focuses on creating something that is generating randomly or at least looks like it does. This was also the theme for this miniex. Our program three rules for our program:

1. If the ellipses have a distance from each other which is windowWidth/5 <= , a colorful line is drawn between them.  
2. If the ellipses have a distance from each other which is windowWidth/5 <= , a new ellipse is drawn in the middle of the two ellipse
3. The time between the birth of a new ellipse is increasing exponentially 


When the program starts two ellipses appear on the screen and the song Fireflies by Owl City starts playing.
These two ellipses are moving randomly around the canvas and once they reach a distance to one another on windowWidth/5 <=, a colorful line is drawn between them and a new ellipse is born/appears in the middle of the colorful line. We have chosen windowWidth because people might have different window sizes when running the program. 

        if (this.position.dist(arrayOfParticles[i].position) < particleImpactDistance && 0 != this.position.dist(arrayOfParticles[i].position)) {
        
        push()
        strokeWeight(3);
        stroke(random(255), random(255), random(255));
        line(arrayOfParticles[i].position.x, arrayOfParticles[i].position.y, this.position.x, this.position.y);
        pop()
        
        let midPointX = (this.position.x + arrayOfParticles[i].position.x) / 2;
        let midPointY = (this.position.y + arrayOfParticles[i].position.y) / 2;
        
        if (this.counter > this.particleBirthDelay && !this.gaveBirthToParticle && !arrayOfParticles[i].gaveBirthToParticle) {
        new particle(midPointX, midPointY, false);
        this.counter = 0;
        this.particleBirthDelay = this.particleBirthDelay*this.particleBirthDelay;
        }
      }
    }
    this.counter++;

There are now three ellipses on the screen. There is also a vague white line between all of the ellipses. This line represents each time two ellipses have been connected. In contrast to the colorful line, the vague white line never goes away showing how every ellipse has at some time been connected to one another. 

        push()
        strokeWeight(1);
        stroke(0,0,0,10);
        line(arrayOfParticles[i].position.x, arrayOfParticles[i].position.y, this.position.x, this.position.y);
        pop()


We made son constraints to how the ellipses were allowed to give birth to new ellipses with.  We have ensured that the ellipse is not able to make new particles with itself.

        if (this.position.dist(arrayOfParticles[i].position) < particleImpactDistance && 0 != this.position.dist(arrayOfParticles[i].position)) {


We have also made some constraints regarding how quickly the program generates new ellipses. Every time a new ellipse has been generated there is a constraint on whenever the two ellipses that “gave birth” to the new ellipse are allowed to make a new one.

        this.gaveBirthToParticle = false;   
        this.particleBirthDelay = 60 * 5


We have done this to ensure that the program does not become too heavy too quickly. 
This continues throughout the program and never stops. The only thing that can make it stop is your program or computer crashing because the program relatively quickly becomes very heavy. 


**Draw upon the assigned reading(s), how does this mini-exercise help you to understand auto generator (e.g control, autonomy, instructions/rules)? Do you have any further thoughts about the theme of this chapter?**


While reading assigned readings and working with our minex we have come to understand how fundamental randomness is in our society. It is also noticeable that what we thought was completely random might not be as random as we think. Especially when working with computational art, the artist has to set up a certain number of rules that the computer has to follow in order to make random art. The question then is; is the art really random if we have to set some rules (rules stand very much in contrast with randomness) for the artwork. In the assigned reading Manfred Mohr stated in an interview:  “I called my work generative art, or occasionally also algorithmic works” (p. 139). This is a great example on how randomness relies on a defined set of rules - in this case it is algorithms and it is often algorithms that control the randomness. 

In working with our own program, we quickly realised that randomness is something that we generate and not something the computer generates. Even though we have used the syntax “random”, we never allowed the random-function to be fully random (we are also aware that it is not truly random). We always defined which parameters the random-function should choose between - we even made rules for the random-function.

Even though we don’t control the visual aspect of the outcome - because or program is randomly generated. We as artists/programmers have control over the rules which the program follows. Therefore one could argue that we in fact are very much in control of the final outcome and that the randomness is only allowed within the defined rules. Once we start the program, the control is out of our hands and the computer does what it is told to do. 

One of the other things that came to our attention is the question who the artist is in generative art. Is the artist the computer/program or the artist him/herself? One could argue that the computer is the true artist because the computer is actually creating the artwork and the artwork looks different every time. But one could also argue that the artist is the human behind the work, because someone has to write the code. We would argue that the computer/program/code becomes a tool for the artist to work with - a material like a pencil or a physical canvas. 
