let arrayOfParticles = []; //Array that contains all the particle instances
let particleImpactDistance; //The distance needed for to particle to give birth to 2 new particles
let song;

function preload() {
  song = loadSound('assets/Fireflies.mp3');//load song: fireflies by Owl City
  particleImpactDistance = windowWidth/5; //This variable is set to 1/5 of the window width so that the visual distance would be the same no matter the resolution of the display
}


function setup() {
  frameRate(60);//We played around with different framerates but went with the standard 60 fps
  song.play();//Playing the song
  createCanvas(windowWidth, windowHeight);
  particle1 = new particle(50, 50, false);//Creates one particle in the top left
  particle2 = new particle(windowWidth - 50, windowHeight -50, false);//Creates one particle in the lower left
}

function draw() {
  clear();//Clear the canvas for previos figures and elements

  //this simply runs all the particles on the canvas.
  for (let i = 0; i < arrayOfParticles.length; i++) {
    arrayOfParticles[i].run();
  }


}

//The Particle Class
class particle {
  constructor(x, y, isStar, velocity) {
    this.isStar = isStar;// The idea was that there was a chance that the spawned particle would be a star instead of a circle but we did not have the time to implement this

    //We did not have time to use this velocity but the idea was that on spawn it should have a stronger initial velocity on birth and then decreasing in power - but it is not implmented yet ;)
    this.position = new p5.Vector(x, y);
    if (velocity) {
      this.velocity = velocity;
    } else {
      this.velocity = new p5.Vector(0, 0);
    }

    this.sizeGoal = random(10, 20);//The size that the particle will have when it is fully grown
    this.currentSize = 0;//The current size f the particle

    this.gaveBirthToParticle = false;//This is to say if a particle have already given birth to another particle - basically we used this to somewheat control the population
    this.particleBirthDelay = 60 * 5;//The initial delay time that must pass before a particle is ready to give birth to another particle again.
    this.counter = 0;// a simple counter

    //The particles spawn with a random speed and direction.
    this.xSpeed = random(-2, 2);
    this.ySpeed = random(-1, 1.5);

    //This is acutally pretty cool little feature - so every time we create a particle it will add itself to the array, so we know that now matter
    //how we choose to create new particles, it will always add itself to "arrayOfParticles".
    arrayOfParticles.push(this)
  }

  //The run function is just so simplify all the animation and action that needs to be called so we just collected it all in one function
  run() {
    this.growAndRenderParticle();
    this.particleWithinRange();
    this.moveParticle();

  }
  //The main purpose of this is t detect and control the action when 2 or more particles are within distance of each other.
  particleWithinRange() {

    for (let i = 0; i < arrayOfParticles.length; i++) { //For loop goes trough all of the particles...

      //Creates a thin somewhat transparent line between every particle on the canvas
      push()
      strokeWeight(1);
      stroke(50,50,50,10);
      line(arrayOfParticles[i].position.x, arrayOfParticles[i].position.y, this.position.x, this.position.y);
      pop()

      //2 arguments needs to be true here for this if statement to be true
      /*

      this.position.dist(arrayOfParticles[i].position) < particleImpactDistance
      If distance from the this particle is less than particleImpactDistance to particle instances in arrayOfParticles, then it is true

       0 != this.position.dist(arrayOfParticles[i].position)
       if the distance from this particle to particles in arrayOfParticles is not 0, then this is true.

       We do this because all of the particles are stored in arrayOfParticles. This also means that when "this" particle checks if there
       are particles within distance it will go trough the whole array. Eventually that means that it will find itself in the array.
       So by checking if the position is exactly the same, the result is basically that it will just skip itself. We do this to avoid that
       the ellipses "give birth" to new ellipses with themselves.

       Let say tha there are 3 particles on the screen
       that means that arrayOfParticles.length = 3

       An example:
       Every particle checks if they can "reach" another particles. We would like to choose one particle as an example - lets say it is number 2

                                                                                              arrayOfParticles[0]
      arrayOfParticles[2]  ----    Checks the distance from every particle in array   --->    arrayOfParticles[1]
                                                                                              arrayOfParticles[2]

      As you see here at some point it be:

      arrayOfParticles[2]  ----    Checks the distance from every particle in array   --->    arrayOfParticles[2]

      Which means that the distance would be 0.
      */
      if (this.position.dist(arrayOfParticles[i].position) < particleImpactDistance && 0 != this.position.dist(arrayOfParticles[i].position)) {

        //
        push()
        strokeWeight(3);
        stroke(random(255), random(255), random(255));
        line(arrayOfParticles[i].position.x, arrayOfParticles[i].position.y, this.position.x, this.position.y);
        pop()

        //Finds the midpoint between the two particles
        let midPointX = (this.position.x + arrayOfParticles[i].position.x) / 2;
        let midPointY = (this.position.y + arrayOfParticles[i].position.y) / 2;

        //Checks if the particle is ready to give birth again
        if (this.counter > this.particleBirthDelay && !this.gaveBirthToParticle && !arrayOfParticles[i].gaveBirthToParticle) {
          new particle(midPointX, midPointY, false);
          this.counter = 0;//Resets the counter
          this.particleBirthDelay = this.particleBirthDelay*this.particleBirthDelay;//This increases the amount of time needed berfore a particle can give birth again
          //this.gaveBirthToParticle = true; //We exsperimented with only allowing one child per particle
        }
      }
    }
    this.counter++;
  }

  growAndRenderParticle() {
    if (!this.isStar) {//If this is not a star then this is true. This is beacuse we wanted to introduce a new shape of the particle ut we did not have the time - so just ignore this...
      if (this.currentSize <= this.sizeGoal) {
        ellipse(this.position.x, this.position.y, this.currentSize)
        this.currentSize = this.currentSize + 0.1;
      } else {
        ellipse(this.position.x, this.position.y, this.sizeGoal)
      }
    }
  }

  moveParticle() {
    if (this.position.x < 0 || this.position.x > width)
      this.xSpeed *= -1;
    if (this.position.y < 0 || this.position.y > height)
      this.ySpeed *= -1;
    this.position.x += this.xSpeed;
    this.position.y += this.ySpeed;
  }
}
