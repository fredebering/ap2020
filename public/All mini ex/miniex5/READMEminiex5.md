**Miniex 5**


**This is my miniex 5 which took inspiration from my miniex 1**
![Screenshot](miniex5.png)


**This is my miniex 1**
![Screenshot](minex1.png)



**Here is the link to my program**
https://fredebering.gitlab.io/ap2020/All%20mini%20ex/miniex5




**Revisit the past**
So for this miniex i tool inspiration from my miniex 1. I wanted to play around with the different objects in my miniex 1 and making them move. 
I wanted to play around with for-loops, because it is a loop that i find a little bit hard to understand and do stuff with. Therefore i used for-loops 
in order to make both the circle and the rectangle move around on the canvas. It is really simple for-loops, but i find it a good place to start. 




    function sqauresMoving (){
    for(let i = 0; i < 3; i++){
    fill(250,250,55);
    noStroke();
    let x = random (width);
    let y = random (height);
    rect(x,y,50,50);
  
  
  
  
    function circlesMove(){
    for(let i = 0; i < 3; i++){
    fill(255,150,0);
    noStroke();
    let x = random (width);
    let y = random (height);
    ellipse(x,y,55,55);
  
  
  
I also wanted to play around with buttons and making a "like counter". 
For the moving black circle i just took inspiration from my throbber, which moves the same way. 




**My own reflections in relation to the course so far**


Understanding programming and being able to program opens up a lot up of opportunities in regard to the worklife, but i also find it to be a great
tool to understand the world we live in to, which is highly dependent on technologi. Programming is something that is in the world to stay
and therefore it is really important for us to understand or at least have an idea about what is does and how it works. There is a very small part of
society which has no to very little knowlegde about this topic, which included me before i started the course. We have very little knowlegde about
our own foodprints on the internet and how the people with power can abbuse and sell the data we are voluntarily providing them with. This i find very 
problematic. That is why i highly agree with Nick Momfort on the fact, that every need to know how to program. By this im not saying that everyone need
to become programmers, but a very small amount of knowlegde can come a long way. 
We have also seen that big bodies like The European Union have resorted in making a law on data in order for the users to have their rights in relation to 
person data safely secured on the internet. 
I find that this goes agains the original thoughts regarding that fact, that the internet was surpose to be a free space for people, but now it is slowly
been more and more controled. And i wonder how the createres of the internet might feel about this development 


I also find that programming requres extreme precision with it syntaxes and leaves little to no space for errors in its language.
This is also something that differs a lot from natural langauge, where people often is able to understand what you are trying to say despite of
spelling errors or incorret grammar
