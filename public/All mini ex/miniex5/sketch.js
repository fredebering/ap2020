var sourceText = "my first sketch";
let l = 117;
let m = 175;
let button
let count = 0;

function setup() {
  createCanvas(600, 400);
  frameRate(20);

//the like button
  button = createButton ('like');
  button.mousePressed(likeCounter);
  button.position(393,312);

}

function draw() {
background('#fae');
sqauresMoving();
circlesMove();
printText();
blackCircle();
printText2();

//the counter
fill(178,102,255);
textSize(50);
text(count,500,260);
}

//the like counter
function likeCounter(){
  count++;
}

// the text "my first sketch". It follows the mouses' position horisontally
function printText(){
fill(0,172,230);
textSize(32);
textAlign(CENTER,CENTER);
var middle = sourceText.length / 2;
var left = middle - ((mouseX / width) * middle);
var right = middle + ((mouseX / width) * middle);
text(sourceText.substring(left,right+1),width/2, 30);
}

//the text " do you like my first sketch"
function printText2(){
  fill(178,102,255);
  textSize(20);
  text('do you ..... my first sketch?',450,320);

}

//using for-loops for the random squares moving around the canvas
function sqauresMoving (){
for(let i = 0; i < 3; i++){
  fill(250,250,55);
  noStroke();
  let x = random (width);
  let y = random (height);
  rect(x,y,50,50);
}
}

//using for-loops for the random circles moving around the canvas
function circlesMove(){
  for(let i = 0; i < 3; i++){
  fill(255,150,0);
  noStroke();
  let x = random (width);
  let y = random (height);
  ellipse(x,y,55,55);
}
}

//the black circle moving arond in a sqaure shape on the canvas
function blackCircle(){
  fill(0);
  if(l <= 185 && m == 175){
    l++;
  }

  if(m<=240 && l ==186){
  m++;
  }

  if(l>=117 && m ==241){
    l--;
  }

  if(m<=241 && l == 117){
    m--;
  }

  ellipse (l, m, 60, 60);
}
