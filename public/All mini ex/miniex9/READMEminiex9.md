**Fortune Cookie**

![ScreenShot](fortunecookie1.png)

![ScreenShot](fortunecookie2.png)

 https://fredebering.gitlab.io/ap2020/All%20mini%20ex/miniex9


This miniex was made in collaboration with Simone, Mads and Røskva


**What is the program about? Which API have you used and why?**


The program first shows a fortune cookie. When you press it, the cookie breaks and inside is a note with an advice. There’s a button that gives you the opportunity to get a new cookie that you can crack to get a new note of advice. The advice is picked randomly from an array within the API from https://api.adviceslip.com/advice. We chose this API because it was really simple and therefore easy to work with. We wanted to challenge ourselves and learn how to work with API’s without overcomplicating it from the very beginning. 


**Can you describe and reflect on your process of making this miniex?**


We didn’t need any API-key to “activate” or use our API. Therefore we didn’t get to practice the use and findings of an API-key. We also reflected upon why we didn’t have to use the key. We found it interesting how some APIs are restricted by the API-key and how some data is found more valuable than others. As we read in the article API practices and paradigms: Exploring the protocological parameters of APIs as key facilitators of sociotechnical forms of exchange (Snodgrass & Soon, 2019) an API-key is used to track who wants to access the API’s data. Because of the lack of need for an API-key one must assume that the data we extracted was not valuable. Or at least that the people who created it did this with no further intentions; the data is open source and free for all to use, no strings attached. Big companies such as Google have many restrictions in their terms and conditions when using and accessing their APIs: “If you do not agree to the modified terms of a Service, you should discontinue your use of that Service” (Google, 2017)”. This shows the great power that some of the big companies’ API’s have.
Because APIs are perceived as building blocks for different platforms to be able to connect with each other - and this need is something we see in almost every platform on the internet - these big companies such as Google or Facebook suddenly gain a lot of power through their APIs. This creates an asymmetry in the exchange of data. The asymmetry appears when someone is giving more data than they receive. It also appears when big companies hide how they categorize their data in the APIs. This is seen in the example of the Google Image Search API where one can’t see how Google decides what is the 10 images in the APIs. 

**Try to formulate a question in relation to web APIs or querying/parsing processes that you want to investigate further if you have more time**


If we had more time, we would like to investigate the queerness of API. Can APIs be queer and to what extent?
