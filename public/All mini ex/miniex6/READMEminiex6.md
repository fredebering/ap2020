Miniex 6

![ScreenShot](miniex6.png)

https://fredebering.gitlab.io/ap2020/All%20mini%20ex/miniex6

**Describe how does your game/game objects work?**
The gameplay is, that one has to control the wok using the right and left key and catch all of the ingredients that is used to make a wok. 
Whenever one has catched a wok, the ingredient-scoreboard goes up and when one ingredient is lost/not caught the wasted-scoreboard goes up. 
One loses the game if the wasted-scoreboard is higher than the ingredient-scoreboard.
At least this was my origianl idea. I had a lot of trouble making the scoreboard work. I also had a lot of trouble making the program
push new ingredients all the time. I found out that my program only prints new ingredients if they are caught by the wok. That means that everytime
an ingredient is lost, it is gone forever and that is not surpose to happen. 



**Describe how you program the objects and their related attributes and methods in your game.**
The objects in my program is images of coconutmilk, carrots and tofus. The example from my code, which is shown beneath is how i made my classes. The only difference in the three classes is, that they show
different images, otherwise they are exactly the same. I started of by making my constructor of what attributes i wanted my object to have. I wanted them 
to have a speed and a position. 
After that a wanted the class to be able to move, so a made a function for that inside the class, which defined the speed of the object. I also wanted my object to be able to be shown
so i made a show function where i defined what the class should show and the size of image. 


    class Tofu {
    constructor (){
        this.speed = floor(random(2,4));
        this.pos = new createVector(random(10,width-15), 0);
    }

    move(){
    this.pos.y += this.speed;
    }

    show(){
    this.image = image(tofu,this.pos.x,this.pos.y, 40,40);
    }
    }

I also made a function that checks when each carrot, coconutmilk and tofu hits the wok. When they hit the wok, they disappear and the ingredient-scoareboard
increases with one. The program then prints a new object whenever one disappear. 


**What are the characteristics of object-oriented programming and the wider implications of abstraction?**
Object-oriented programming, which is the main theme and readings for this miniex, is a way for one to organize the code. This is done by defining
objects (images, ellipses self-drawn objects ect.) and applying data to these objects. It focuses more on data or objects rather than logic or funtions
This is great way to organize code because the data which is being applyed to the objects can be encapsulated. This makes it easier to reuse code and apply
them to other objects as well. 
This way of encapsulating data can be done using classes. A class  A class specifies the structure of its objects' attributes and the possible behaviors/actions of its objects.
It is the programmer who decides what attributes is relevant for the specific object. In my program i chose to different properties such as:
speed and a position on the canvas. I also gave them different behaviors which included them being able to move around the canvas and i also wanted them
to show on the canvas. It is very important to be aware about what attributes and abstractions one is making when creating a program. For this miniex
the effects of the choses that i have made might not have been very influencial because i made a very basic program that didn't even work. But
in a wider context these abstractions can be very influencial.


**Extend/connect your game project to wider digital culture context, can you think of a digital example and describe how complex details and operations are being abstracted?**
As briefly mentioned i think it is very important to be aware of what attributes and abstractions programmers make when creating new games or even just as simple as creating 
emojis (even though i am aware that emojis is not object-oriented pogramming). Seen in a wider cultural context, these decisions might have a greater influence on how we percieve
the world. Here my thoughts go to AI (artficial intelligence) and how we program a human being. What characterizes us as humans? Is it the way we look? Is it our gender?
Is it our personality? Or is a mixture of all of these things? And if so, how do we programs these different characteristics? How gets to decide what is defining us and what is not.
This is some of the questions that is related to object-orinted programming because this way of programming comes with a lot of choose what one think is relevant to the object
and what is not. This can also be seen in our readings about emojis and the problematics regarding the different facial expressions or the skincolors. To we all look the same
whem we are angry or when we are happy? And who is to decide what is the "right" or "common" facial expression when looking angry?
Because programming is becomming intertwined with our everyday life it is important to be able to see and understand some of the choices that are being made for us and what
effect they can have on the way we percieve the world. A great example of this is the gaming world. Here stereotypes are often being put to the extreme. On female charaters in gaming
the features which is being used to define a woman is being amplified in a ridiculous way. Here i mostly think of breasts. If all you do i sit inside and game all day long
instead of seeing what people actually look like in the real world, one might get a wrong or twisted sight on gender. 
There is no doubt that we are highly influenced by the techonologi we use every day and therefore it is important to be critical and aware of the choices that are being made behind the scenes

