let coconutMilkFall;
let tofuFall;
let carrotFall;
let noodlesFall;
let carrotr = [];
let tofur = [];
let coconutr = [];
let min_carrot = 3;
let min_tofu = 3;
let min_coconut = 2;
let wokPosX;
let score =0, lose = 0;
let wokSize = {
  w: 250,
  h: 250
};


function preload () {
  kitchen = loadImage('assets/køkken.png');
  carrot = loadImage('assets/carrot.png');
  coconutmilk = loadImage('assets/kokusmælk.png');
  tofu = loadImage('assets/tofu.png');
  wok = loadImage('assets/wok.png');
  }

function setup() {
createCanvas(700,500);
wokPosX = 350;
for (let i = 0; i < min_carrot; i++){
  carrotr[i] = new Carrot ();
    }

 for (let i = 0; i < min_tofu; i++){
   tofur[i] = new Tofu ();
  }

 for (let i = 0; i < min_coconut; i++){
   coconutr[i] = new Coconut ();
 }
}

function draw() {
background(kitchen);
movingWok();
showCarrot();
checkCarrot();
showTofu();
checkTofu();
showCoconutmilk ();
checkCoconut ();
checkCarrotCaught();
checkTofuCaught();
checkCoconutCaught();
displayScore();
checkResult();
}

//making it show the carrot and printing a new one when a carrot is out of the canvas
function showCarrot (){
for (let i = 0; i < carrotr.length; i++){
  carrotr[i].show();
  carrotr[i].move();
  if (carrotr[i].pos.y > height){
    //carrotr.splice(i,1);
    }
  }
}

//checking and pushing a new carrot on to the canvas
function checkCarrot (){
  if (carrotr.length < min_carrot){
    carrotr.push(new Carrot());
  }
}

//making it show the carrot and printing a new one when a tofu is out of the canas
function showTofu (){
  for (let i = 0; i < tofur.length; i++){
    tofur[i].show();
    tofur[i].move();
    if(tofur[i].pos.y > height){
      //tofur.splice(i,1);
    }
  }
}
//checking and pushing a new tofu on to the canvas
function checkTofu (){
  if (tofur.length < min_tofu){
    tofur.push(new Tofu());
  }
}
//making it show the coconut and printing a new one when a coconut is out of the canas
function showCoconutmilk (){
  for (let i = 0; i < coconutr.length; i++){
    coconutr[i].show();
    coconutr[i].move();
    if(coconutr[i].pos.y > height){
      //coconutr.splice(i,1);
    }
  }
}

//checking and pushing a new coconut on to the canvas
function checkCoconut (){
if (coconutr.length < min_coconut)
  coconutr.push(new Coconut());
}

function checkCarrotCaught() {
  //trying to calculate the distance between each carrot
 for (let i = 0; i <tofur.length; i++) {
    let d = int(dist(wokPosX, 390, tofur[i].pos.x, tofur[i].pos.y));
    if (d < wokSize.w/3) {
      score++;
      tofur.splice(i,1);
    }else if (tofur[i].pos.y <= 0) {
      lose++;
    //  carrotr.splice(i,1);
    }
  }
}

function checkTofuCaught() {
  //trying to calculate the distance between each tofu
 for (let i = 0; i <carrotr.length; i++) {
    let d = int(dist(wokPosX, 390, carrotr[i].pos.x, carrotr[i].pos.y));
    if (d < wokSize.w/3) {
      score++;
      carrotr.splice(i,1);
    }else if (carrotr[i].pos.y <= 0) {
      lose++;
    //  carrotr.splice(i,1);
    }
  }
}

function checkCoconutCaught() {
  //trying to calculate the distance between each coconutmilk
 for (let i = 0; i <coconutr.length; i++) {
    let d = int(dist(wokPosX, 390, coconutr[i].pos.x, coconutr[i].pos.y));
    if (d < wokSize.w/3) {
      score++;
      coconutr.splice(i,1);
    }else if (coconutr[i].pos.y <= 0) {
      lose++;
    //  carrotr.splice(i,1);
    }
  }
}


//the wok follows the mouse
function movingWok (){
  image (wok, wokPosX, 270, wokSize.w, wokSize.h);
}



//Setting up a class for every carrot
class Carrot {
  constructor (){
    this.speed = floor(random(3,5));
    this.pos = new createVector(random(10,width-40), 0);
  }

move(){
  this.pos.y += this.speed;
}
show(){
  this.image = image(carrot,this.pos.x,this.pos.y, 100,70);
  }
}

//Setting up a class for every tofu
class Tofu {
  constructor (){
    this.speed = floor(random(2,4));
    this.pos = new createVector(random(10,width-15), 0);
  }

move(){
  this.pos.y += this.speed;
}

show(){
  this.image = image(tofu,this.pos.x,this.pos.y, 40,40);
  }
}

//Setting up a class for every coconutmilk
class Coconut {
  constructor (){
    this.speed = floor(random(2,5));
    this.pos = new createVector(random(10,width-15), 0);
  }

move(){
  this.pos.y += this.speed;
}
show(){
  this.image = image(coconutmilk,this.pos.x,this.pos.y, 30,40);
  }
}

function displayScore() {
    fill(255, 102, 0);
    textSize(17);
    text('You have caught '+ score +  " ingredients", 10, 20);
    text('You have wasted '+ lose +  " ingredients", 10, 40);
    fill(204, 0, 255);
    text('Press the RIGHT or LEFT key to catch the ingredients', 10, 60)
}

function checkResult() {
  if (lose > score && lose > 4  ) {
    fill(255,0,0);
    textSize(26);
    text("TOO MUCH WASTAGE", 150, height/1.4);
    noLoop();
  }
}

function keyPressed() {
  image (wok, wokPosX, 270, wokSize.w, wokSize.h);
   if (keyCode === LEFT_ARROW) {
     wokPosX-=55;
   } else if (keyCode === RIGHT_ARROW) {
     wokPosX+=55;
   }
 }
