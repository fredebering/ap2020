function setup() {
  createCanvas(600, 400);
}
//Emoji #1
function draw() {
  background(225,225,225);
  fill(255,204,0);
  noStroke();
  ellipse(200,200,200,200);

//eyes
fill(255,150,0);
  noStroke ();
  ellipse(170,180,40,55);
  ellipse(235,180,40,55);

//mouth
 noFill(255,150,0);
  stroke(255,150,0);
  strokeWeight(6);
 arc(200, 200, 110, 110, QUARTER_PI,PI-QUARTER_PI)

//Emoji #2
  fill(136,116,152);
  noStroke();
  ellipse(450,200,200,200);

//eyes
  fill(255,150,0);
  noStroke ();
  ellipse(480, 180,35,35);
  ellipse(420,180,35,35);

//mouth
  fill(255,150,0);
  stroke(230,150,2);
  strokeWeight(2);
  fill(255,255,255);
  rect(390,220,120,35,140);
  strokeWeight(1);
  line(410,254,410,220);
  stroke(230,150,0);
  line(430,254,430,221);
  line(470,254,470,221);
  line(490,254,490,221);
  line(450,254,450,221);
  strokeWeight(2);
  line(390,237,509,237);
}
