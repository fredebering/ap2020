Mini ex2

![ScreenShot](mini2.png)

https://fredebering.gitlab.io/ap2020/All%20mini%20ex/miniex2

**Describe your program and what you have used and learnt.**
My program shows to emojis. One of the emojis is a classic yellow "skintoned" emoji as we all know a use and the other one is my take on an alternative purple "skincolor". 
I have chosen to give the other emoji an alternative "skintone" because i wanted to reflect opon what it means to the emoji that the skintone is yellow and whether or not it would be possible to have chosen another color and still maintain the neutrality of the emoji. I also wanted to challenge whether or not the yellow color really is neutral or not. I have often heard people talking about Asian people as people with yellow skintones, so is the yellow really neutrual or does it reflect a sertain "race" after all?
I didn't chose the purple color to make a sertain statement. It might as well have been blue or grey. But i think is it interesting to investigate the different alternative skintones. Thus i am aware that purple is often seens as a feminine color and therefore might not bee as neutrual is i first stated. By what if this purple was the standard color of the classical emoji to begin with? Would we then have stated it as a neutrual color? As far as i'm aware, no "races", identities ect. is stereotyped as having a purple skintone. So this unrealistic alternative might be the best way to maintain diversty because no one can relate directly to the color?
These are just few of the many questions i asked myself when reading the text "Modifying the Universal" by Abbing, Pierrot and Snelting and i was very intrigued when they talked about how complex diversity is and if we, in an attempt to create diversty in reality is generalizing and making steorotypes. So why not make the emojis so unrealistic, at least when it comes to skincolor and only focus on making the facical expressions relatable
